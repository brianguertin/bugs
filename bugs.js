var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, '', { preload: preload, create: create, update: update });

function preload() {
	game.load.image('title', 'title-small.png');
	game.load.image('grass', 'grass.jpg');
	game.load.spritesheet('fly', 'fly.png', 177, 144);
	game.load.audio('squish', 'squish.wav');
	game.load.audio('buzz', 'buzz.wav');
}

var scope = {
	maxFlies: 2,
	fontSettings: { font: "20px Arial", fill: "#000000", align: "center", shadowColor: "#ffffff", shadowBlur:2 },
	flyRadius: 80
};

function createFly() {
    var fly = scope.flies.create(scope.flyRadius + Math.random() * (game.width - scope.flyRadius * 2), scope.flyRadius + Math.random() * (game.height - scope.flyRadius * 2), 'fly');
    fly.id = scope.flies.length;
    fly.animations.add('fly', [0,1]);
    fly.animations.add('dead', [2]);
    fly.anchor.setTo(0.5, 0.5);
    fly.animations.play('fly', 15, true);
    fly.inputEnabled = true;
    fly.events.onInputDown.add(onFlyTouched, {fly:fly}); // TODO: Smaller hit box
    
    var scale = 0.3 / ((fly.width / game.width) + (fly.height / game.height));
    fly.scale.setTo(scale, scale);
    
    game.physics.enable(fly, Phaser.Physics.ARCADE);
    fly.body.collideWorldBounds = true;
    fly.body.bounce.set(1);
    
    var target = {x:Math.random() * game.width, y:Math.random() * game.height};
    game.physics.arcade.moveToXY(fly, target.x, target.y, 250 * scale);
    setRotationFromVelocity(fly);
    
	scope.buzzing.play();
	
	if (scope.flies.length == scope.maxFlies) {
		scope.waitText.destroy();
	}
}

function create() {	
    game.physics.startSystem(Phaser.Physics.ARCADE);

    game.add.tileSprite(0, 0, game.width, game.height, 'grass');
    
	scope.squishAudio = game.add.audio('squish');
	scope.buzzing = game.add.audio('buzz');
    scope.flies = game.add.group();
    
    scope.title = game.add.image(game.width / 2, game.height / 2 - 70, 'title');
    scope.title.anchor.setTo(0.5, 0.5);
				
	scope.instructions = game.add.text(game.width / 2, game.height / 2 + 40, '[Play]', scope.fontSettings);
	scope.instructions.anchor.setTo(0.5, 0);
	
	game.input.onDown.add(function() {
		if (scope.instructions) {
			scope.instructions.destroy();
			scope.instructions = null;
			
			if (scope.title) {
				scope.title.destroy();
				scope.title = null;
			}
			startRound();
		}
	}, this);
}

function update () {
	scope.flies.forEach(function(fly) {
		setRotationFromVelocity(fly);
	});
}

function startRound() {
	scope.flies.removeAll(true);
	scope.deadFlies = 0;
	game.time.events.repeat(Phaser.Timer.SECOND * 2, scope.maxFlies, createFly, this);
	
	scope.waitText = game.add.text(game.width / 2, game.height / 2, 'Squish the bugs in the\norder they appear.\n\nWait for ' + scope.maxFlies + ' bugs...', scope.fontSettings);
	scope.waitText.anchor.setTo(0.5, 0.5);
}

function setRotationFromVelocity(sprite) {
	sprite.rotation = game.math.angleBetween(0, 0, sprite.body.velocity.x, sprite.body.velocity.y) + Math.PI / 2;
}

function onFlyTouched(e) {
	if (!scope.instructions && scope.flies.length == scope.maxFlies && !this.fly.isDead) {
		this.fly.isDead = true;
		++scope.deadFlies;
		scope.squishAudio.play();
		this.fly.animations.play('dead');
		scope.flies.sendToBack(this.fly);
		this.fly.body.velocity.setTo(0, 0);
		
		if (scope.deadFlies == scope.maxFlies) {
			++scope.maxFlies;
			scope.instructions = game.add.text(game.width / 2, game.height / 2, 'Good job!\n\n[Next level]', scope.fontSettings);
			scope.instructions.anchor.setTo(0.5, 0.5);
			
		} else if (this.fly.id !== scope.deadFlies) {
			scope.instructions = game.add.text(game.width / 2, game.height / 2, 'Whoops!\nYou squished the wrong bug.\n\n[Try again]', scope.fontSettings);
			scope.instructions.anchor.setTo(0.5, 0.5);
		}
	}
}
